import scipy
import scipy.signal
import matplotlib.pyplot as plt

plt.ioff() # dont autoshow images
logf = open("Output.txt", "w")
from scipy.io import wavfile as wav
from scipy.fftpack import fft
import numpy as np

import pandas as pd
import random

#import librosa
#import librosa.display
import pylab

from multiprocessing import Process
from os import walk
import os
from enum import Enum
import traceback

import wavefile
from subprocess import check_output

class WindowMode(Enum):
    Random = 1
    Overlap = 2
    Concatenated = 3

def full_frame(width=None, height=None):
    import matplotlib as mpl
    mpl.rcParams['savefig.pad_inches'] = 0
    figsize = None if width is None else (width, height)
    fig = plt.figure(figsize=figsize)
    ax = plt.axes([0,0,1,1], frameon=False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.autoscale(tight=True)

def combineHandcraftedFeatures(outputfolder,file_name):
    f = []
    for (dirpath, dirnames, filenames) in walk(outputfolder):
        f.extend(filenames)
        break

    df_full = pd.DataFrame();
    lastFileName = "_"
    for filename in f:
        if str.startswith(filename,"tmp_"):
            df_full = df_full.append(pd.read_csv(outputfolder + "\\" + filename, sep=";"))
            lastFileName = filename
    df_full = df_full.reset_index(drop = True)
    df_full["name"] = df_full["name"].str[1:-1]
    df_full.drop('frameTime', axis=1, inplace=True) # always 0.000000
    parts = df_full["name"].str.split("_", expand=True)
    p_length= len(parts.columns) -2
    df_full["name"] = parts[pd.RangeIndex(0,p_length,1)].apply(lambda x: '_'.join(x), axis=1)
    df_full.insert(1, "start", parts[p_length ])
    df_full.insert(2, "end", parts[p_length + 1])
    df_full.to_csv(outputfolder + "\\" + file_name + ".csv",index=False)
    for filename in f:
        if str.startswith(filename,"tmp_"):
            os.remove(outputfolder + "\\" + filename)

    
def combineSpectogramSummaries(outputfolder):
    f = []
    for (dirpath, dirnames, filenames) in walk(outputfolder):
        f.extend(filenames)
        break
    
    df_full = pd.DataFrame();
    for filename in f:
        if str.startswith(filename,"generatedSummary"):
            df_full = df_full.append(pd.read_csv(outputfolder + "\\" + filename));
            os.remove(outputfolder + "\\" + filename)
    df_full.to_csv(outputfolder + "\\generatedSummary_combined.csv",index=False)

class GenerateTrainingData():
    def __init__(self, windowlengthMS, outputfolder, summary, wavFolder,windowMargins=(0,0), windowMode=WindowMode.Concatenated, overlap=0):
        self.outputfolder = outputfolder
        self.windowlengthMS = windowlengthMS
        self.summary = summary
        self.wavFolder = wavFolder
        self.windowMode = windowMode
        self.windowMargins = windowMargins
        self.overlap = overlap
        
    def _getWindowBorders(self,totalLength,count,rate):
        msToRate = rate / 1000
        startOfRange = self.windowMargins[0] * msToRate
        fullLength = totalLength - (self.windowMargins[1] * msToRate)
        endOfRange = fullLength -  int((msToRate * self.windowlengthMS))
        
        returnMe = []
        if self.windowMode == WindowMode.Random:
            for i in range(count):
                start = random.randint(startOfRange,fullLength)
                end = int(start + msToRate * self.windowlengthMS)
                returnMe.append((start,end))
        elif self.windowMode == WindowMode.Overlap or self.windowMode == WindowMode.Concatenated:
            if self.windowMode == WindowMode.Concatenated:
                self.overlap = 0
            i = 0
            while(i<count) and (startOfRange < endOfRange):
                start = startOfRange;
                end = startOfRange + int((msToRate * self.windowlengthMS))
                returnMe.append((start,end))
                
                i=i+1;
                startOfRange = startOfRange + int((msToRate * self.windowlengthMS)) - (self.overlap * msToRate);
                
        return returnMe;

    def _genSpectogramm(self,wavPath,count,specType="normal"):
        windowLengthMS = self.windowlengthMS
        savFolder = self.outputfolder;

        filename = str.split(wavPath,"\\")[-1]
        filename = str.split(filename,".")[0]

        data = rate = 0

        if specType == "normal":
            rate, data = wav.read(wavPath)

        elif specType == "mel":
            data, rate = librosa.load(wavPath)
        
        windows = self._getWindowBorders(len(data),count,rate)
        filenames = []
        
        for window in windows:
            start =  int(window[0])
            end =  int(window[1])

            nextFilename = filename + "_" + str(start) + "_" + str(end) +".png"
            fullPath = savFolder + "\\" + nextFilename

            full_frame()

            if specType == "normal":
                f, t, Sxx = scipy.signal.spectrogram(data[start:end], fs=rate,noverlap=100)
                plt.pcolormesh(t, f, Sxx)
                plt.savefig( fullPath, bbox_inches='tight', pad_inches=0)

            elif specType == "mel":
                S = librosa.feature.melspectrogram(y=data[start:end], hop_length=100, sr=rate, n_mels=128, fmax=8000)
                librosa.display.specshow(librosa.power_to_db(S, ref=np.max))
                pylab.savefig(fullPath, bbox_inches='tight', pad_inches=0)

            plt.close();
            filenames.append(nextFilename)
        return filenames;
    
    def _callOpenSmile(self, processNumber, wavFolder, wavPath, count, configuration_file):

        w =  wavefile.load(wavFolder + "\\" + wavPath)
        fs = w[0]
        data = w[1]
        
        windows = self._getWindowBorders(len(data[0]),count,fs)
        
        filenames = []
        df_ = pd.DataFrame();
        for window in windows:
            
            start =  int(window[0])
            end =  int(window[1])
            
            output = check_output(["C:\\Users\\lukas\\Documents\\Masterarbeit_Programme\\opensmile-2.3.0\\bin\\Win32\\SMILExtract_Release.exe", "-C", "C:\\Users\\lukas\\Documents\\Masterarbeit_Programme\\opensmile-2.3.0\\config\\" + configuration_file, "-csvoutput", self.outputfolder + "\\tmp_" + str(processNumber) + "_handcrafted.csv" , "-I" , wavFolder + "\\" + wavPath, "-start", str(start / fs), "-end", str(end / fs), "-instname", wavPath.split(".")[0] + "_" + str(start) + "_" + str(end), "-frameModeFunctionalsConf", "C:\\Users\\lukas\\Documents\\Masterarbeit_Programme\\opensmile-2.3.0\\config\\shared\\CustomFrameMode.conf.inc", "-loglevel", "1"])
            
                
    def multiProcessOpenSmile(self, processNumber,numOfProcesses,featureSetPerFile,configuration_file="ComParE_2016.conf"):
        try:            
            for i in self.summary.index[processNumber::numOfProcesses]:
                self._callOpenSmile(processNumber,self.wavFolder,self.summary.loc[i,"filename"],featureSetPerFile, configuration_file)
                    
        except Exception as e:
            logf.write("Failed: " + str(e))
    
    def multiProcessRunMelFilterbank(self,processNumber,numOfProcesses,picturesPerFile):
        self._multiProcessRun(processNumber,numOfProcesses,picturesPerFile,"mel");

    def multiProcessRunSpectogram(self,processNumber,numOfProcesses,picturesPerFile):
        self._multiProcessRun(processNumber,numOfProcesses,picturesPerFile,"normal");

    def _multiProcessRun(self, processNumber,numOfProcesses,picturesPerFile,specType):
        try:
            df = pd.DataFrame();
            for i in self.summary.index[processNumber::numOfProcesses]:
                generatedFiles = self._genSpectogramm(self.wavFolder + "\\" + self.summary.loc[i,"filename"], picturesPerFile,specType)
                    
                if len(generatedFiles)>0:
                    x = self.summary.iloc[i]
                    for filename in generatedFiles:
                        t = x.copy()
                        t["generatedFilePath"] = filename
                        df = df.append(t)
                        
            df.to_csv(self.outputfolder +"\\generatedSummary" + str(processNumber) + ".csv", index=False)

        except Exception as e:
            
            logf.write('Caught exception in worker thread (x = %d):' % processNumber)
            logf.write(str(e))
            # This prints the type, value, and stack trace of the
            # current exception being handled.
            traceback.print_exc(file="Output.txt")

