# Content

## classify gender / age from spectogram images

### 1. clean agender data with agender.ipynb
- converts the raw audio files to .wav

### 2. (optional) remove audio segements containing no audio using webrtcvad

the example below shows 1001_2_a11001s4.wav before and after trimming. (plot example in exploration/pyvad/pyvad_example.ipynb)

![alt text](exploration/pyvad/pyvadTrimExample_1.png)
![alt text](exploration/pyvad/pyvadTrimExample_2.png)

using pyvad with maximum aggressiveness mode for filtering audio data removes about half the audio data.

Before: 38.04 hours
After: 19.95 hours

pyvad.ipynb cleans the whole directory

### 3. generate Spectograms with spectogrammmHelper.py
- spectogrammmHelper is used in generateTrainingDataFromWavs.ipynb
- uses multiple processes and runs quite fast
- also generates handcrafted features (mffccs)

### 4. specify training Target in SplitNCutSpectograms.ipynb
- generates train / validation split
- specify training tatget (gender, age ....)

### 5. use VGG16 to create bottleneck feautres
- run genenerate bottleneck features of BottleneckFeatures.ipynb
![alt text](exploration/readmeImages/vgg16_original.png)

### 6. recreate top Flatten and Dense Layers and search for fitting hyperparameters

#### 6.1 shallow top net Male / Female / Child

classes:

child = age<=14
male = age>14 & gender=m
female = age>14 & gender=f

VGG16 uses 1 flatten and 3 dense layers with 4096 neurons and relus.

For faster iteration times the grid search is capped at 256 neurons and 4 hidden layers with a maximum of 10 epochs per run. (used parameters are in results/shallowTopModel )

The Neural Network overfits after a few epochs
![alt text](exploration/readmeImages/shallowTopModelTrain.png)

Classification Report

|             | precision | recall | f1-score | support |
|-------------|-----------|--------|----------|---------|
| child       | 0.49      | 0.48   | 0.49     | 5231    |
| male        | 0.87      | 0.89   | 0.88     | 13516   |
| female      | 0.75      | 0.75   | 0.75     | 15143   |
| avg / total | 0.76      | 0.76   | 0.76     | 33890   |


The confusion matrix shows the network has difficulties ditingushing females from childs while males are recogniced pretty well.

![alt text](exploration/readmeImages/shallowTopModelConfusionMatrix.png)

### 6.2 shallow top net child (young/adult/senior) male (young/adult/senior) females

same parameters as 6.1 just with 7 classes

classes:

child = age<=14

youth_male = gender==m & age >14 & age<25
youth_female = gender==f & age >14 & age<25

adult_male = gender==m age>25 & age<55
adult_female = gender==m age>25 & age<55

senior_male = gender==m & age>55
senior_female = gender==f& age>55

|               | precision | recall | f1-score | support |
|---------------|-----------|--------|----------|---------|
| child         | 0.43      | 0.53   | 0.47     | 5231    |
| male_young    | 0.30      | 0.54   | 0.38     | 3465    |
| male_adult    | 0.31      | 0.22   | 0.26     | 3976    |
| male_senior   | 0.51      | 0.45   | 0.48     | 6075    |
| female_young  | 0.29      | 0.39   | 0.33     | 4481    |
| female_adult  | 0.28      | 0.15   | 0.20     | 4743    |
| female_senior | 0.46      | 0.32   | 0.38     | 5919    |
|               |           |        |          |         |
| avg / total   | 0.38      | 0.37   | 0.37     | 33890   |

![alt text](exploration/readmeImages/confusionMatrix_7class_500ms.png)


Results can be improced by ~5% accuracy by averaging over all spectograms generated for one sound file:

Classification Report Mean

|               | precision | recall | f1-score | support |
|---------------|-----------|--------|----------|---------|
| child         | 0.45      | 0.62   | 0.52     | 1238    |
| male_young    | 0.34      | 0.62   | 0.44     | 924     |
| male_adult    | 0.32      | 0.21   | 0.26     | 1047    |
| male_senior   | 0.60      | 0.53   | 0.56     | 1440    |
| female_young  | 0.31      | 0.43   | 0.36     | 1134    |
| female_adult  | 0.35      | 0.14   | 0.20     | 1279    |
| female_senior | 0.54      | 0.40   | 0.46     | 1452    |
|               |           |        |          |         |
| avg / total   | 0.43      | 0.42   | 0.41     | 8514    |

![alt text](exploration/readmeImages/confusionMatrix_7class_mean.png)

The sound files do not have the same length, therefore some files are classified with just one spectogram while some files are classified with up to 30 spectograms.

in general averaging more validation results does increase the accuracy of the classification
![alt text](exploration/readmeImages/number_of_spectograms.png)
